import { Component, Input, Output, EventEmitter } from '@angular/core';
import { filter, concatMap } from 'rxjs/operators';
import { Country } from '../profile/country';
import * as jstz from 'jstimezonedetect';
import * as timezones from 'timezones.json';
import { DialogService } from '../dialog/dialog.service';
import { UserService } from './user.service';

@Component({
  selector: 'user-form',
  templateUrl: './user-form.component.html',
  styleUrls: [
    './user-form.component.styl'
  ]
})
export class UserFormComponent {

  @Input() model: any = {};
  @Output() modelChange: EventEmitter<any> = new EventEmitter<any>();
  @Input() admin = false;
  timezones = timezones;

  countryList = Country.list;
  sounds: Object[] = [
    { name: 'Bell 1', src: '/assets/audio/bell.mp3'},
    { name: 'Bell 2', src: '/assets/audio/bell1.mp3'},
    { name: 'Birds', src: '/assets/audio/birds.mp3'},
    { name: 'Bowl', src: '/assets/audio/bowl.mp3'},
    { name: 'Gong', src: '/assets/audio/gong.mp3'},
    { name: 'Notification Sound', src: '/assets/audio/solemn.mp3'}
  ];
  currentSound;

  constructor(
    private dialog: DialogService,
    private userService: UserService
  ) {}

  playSound() {
    if (this.model.sound) {
      this.currentSound = new Audio(this.model.sound);
      this.currentSound.play();
    }
  }

  stopSound() {
    if (this.currentSound) {
      this.currentSound.pause();
      this.currentSound.currentTime = 0;
    }
  }

  detectTimezone() {
    const detectedUtcName = jstz.determine().name();
    const detectedTz = (<any>this.timezones)
      .filter(tz => tz.utc ? tz.utc.indexOf(detectedUtcName) > -1 : false)
      .reduce(tz => tz);

    if (detectedTz) {
      this.model.timezone = detectedTz.value;
      return;
    }

    this.dialog.alert('Error', 'Your timezone was not detected.');
  }

  uploadAvatar(evt = null) {
    if (evt) {
      evt.preventDefault();
    }

    this.dialog.uploadAvatar(this.admin && this.model._id)
      .pipe(filter(val => val.length > 0))
      .subscribe(val => this.model.avatarImageToken = val);
  }

  deleteAvatar(evt = null) {
    if (evt) {
      evt.preventDefault();
    }

    this.dialog.confirmDelete().pipe(
      filter(val => !!val),
      concatMap(() => this.userService.deleteAvatar(this.model._id))
    ).subscribe(() => this.model.avatarImageToken = '');
  }
}
