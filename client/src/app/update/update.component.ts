import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../reducers';
import { SetTitle } from '../actions/global.actions';

@Component({
  selector: 'update',
  templateUrl: './update.component.html',
})
export class UpdateComponent {

  constructor(store: Store<AppState>) {
    store.dispatch(new SetTitle('Update'));
  }
}
