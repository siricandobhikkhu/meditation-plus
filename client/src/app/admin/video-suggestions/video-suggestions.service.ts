import { Injectable } from '@angular/core';
import { ApiConfig } from '../../../api.config';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class VideoSuggestionsService {

  public constructor(private http: HttpClient) {
  }

  public getAll(): Observable<any[]> {
    return this.http.get<any[]>(ApiConfig.url + '/api/video-suggestion');
  }

  public accept(suggestion): Observable<any> {
    return this.http.post(
      ApiConfig.url + '/api/video-suggestion/' + suggestion._id,
      '',
      { observe: 'response', responseType: 'text' }
    );
  }

  public delete(suggestion): Observable<any> {
    return this.http.delete(ApiConfig.url + '/api/video-suggestion/' + suggestion._id);
  }
}
