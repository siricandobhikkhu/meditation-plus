import { Component, OnInit, OnDestroy } from '@angular/core';
import { AppointmentService } from './appointment.service';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../user/user.service';
import { SettingsService } from '../shared/settings.service';
import * as moment from 'moment-timezone';
import 'scriptjs';
import * as timezones from 'timezones.json';
import { filter, take, concatMap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { SetTitle, ThrowError } from '../actions/global.actions';
import { AppState } from '../reducers';
import { Observable } from 'rxjs';
import { selectId, selectAdmin } from '../auth/reducders/auth.reducers';
import { DialogService } from '../dialog/dialog.service';
import { MatSnackBar } from '@angular/material';


@Component({
  selector: 'appointment',
  templateUrl: './appointment.component.html',
  styleUrls: [
    './appointment.component.styl'
  ]
})
export class AppointmentComponent implements OnInit, OnDestroy {

  appointments: any;
  appointmentSocket;
  loadedInitially = false;
  userHasAppointment = false;
  currentTab = 'table';

  localTimezone: string;
  rootTimezone = 'America/Toronto';
  rootTimezoneShort = moment.tz(this.rootTimezone).zoneName();

  profile;

  userId$: Observable<string>;
  admin$: Observable<boolean>;

  constructor(
    private appointmentService: AppointmentService,
    private route: ActivatedRoute,
    private userService: UserService,
    private settingsService: SettingsService,
    private dialog: DialogService,
    private snackbar: MatSnackBar,
    private store: Store<AppState>
  ) {
    store.dispatch(new SetTitle('Schedule'));
    this.userId$ = store.select(selectId);
    this.admin$ = store.select(selectAdmin);

    this.route.params
      .pipe(
        filter(res => res.hasOwnProperty('tab'))
      )
      .subscribe(res => this.currentTab = (<any>res).tab);

    this.settingsService.get()
      .subscribe(res => {
        this.rootTimezone = res.appointmentsTimezone;
        this.rootTimezoneShort = moment.tz(this.rootTimezone).zoneName();
      });

  }

  getCurrentMoment(): moment.Moment {
    return moment.tz(this.rootTimezone);
  }
  /**
   * Method for querying appointments
   */
  loadAppointments(): void {
    this.userHasAppointment = false;
    this.appointmentService.getAll()
      .subscribe(
        res => this.appointments = res,
        null,
        () => this.loadedInitially = true
      );
  }

  /**
   * Registration handling
   */
  toggleRegistration(evt, appointment) {
    evt.preventDefault();

    this.userId$.pipe(
      take(1),
      filter(id => !appointment.user || appointment.user._id === id),
      concatMap(() => this.appointmentService.registration(appointment))
    )
    .subscribe(() => {
      this.snackbar.open('Your registration has been toggled.');
      this.loadAppointments();
    }, error => this.store.dispatch(new ThrowError({ error }))
  );
  }

  /**
   * Admin can remove registered appointments
   */
  removeRegistration(evt, appointment) {
    evt.preventDefault();

    this.admin$.pipe(
      take(1),
      filter(val => !!val),
      concatMap(() => this.dialog.confirmDelete()),
      filter(val => !!val),
      concatMap(() => this.appointmentService.deleteRegistration(appointment))
    ).subscribe(
      () => {
        this.snackbar.open('The registration has been removed.');
        this.loadAppointments();
      },
      error => this.store.dispatch(new ThrowError({ error }))
    );
  }

  /**
   * Converts hour from number to string.
   * @param  {number} hour EST/EDT hour from DB
   * @return {string}      Local hour in format 'HH:mm'
   */
  printHour(hour: number): string {
    hour = hour < 0 || hour >= 2400 ? 0 : hour;

    const hourStr = '0000' + hour.toString();
    return hourStr.substr(-4, 2) + ':' + hourStr.substr(-2, 2);
  }

  /**
   *  Tries to identify the user's timezone
   */
  getLocalTimezone(): string {
    if (this.profile && this.profile.timezone) {
      // lookup correct timezone name from profile model
      for (const k of <any>timezones) {
        // check if timezone is compatible with moment-timezone
        if (k.value === this.profile.timezone && moment().tz(k.utc[1])) {
          return k.utc[1];
        }
      }
    }

    // guess timezone otherwise
    return moment.tz.guess();
  }

  /**
   * Converts EST/EDT datetime by hour to UTC
   */
  getUtcHour(hour: number) {
    return moment.tz(this.printHour(hour), 'HH:mm', this.rootTimezone).tz('UTC').format('HH:mm');
  }

  /**
   * Converts EST/EDT hour to local hour.
   * @param  {number} hour EST/EDT hour
   * @return {string}      Local hour
   */
  getLocalHour(hour: number): string {
    if (!this.localTimezone) {
      this.localTimezone = this.getLocalTimezone();
    }

    // read time for EST/EDT timezone
    const eastern = moment.tz(this.printHour(hour), 'HH:mm', this.rootTimezone);
    const local = eastern.clone().tz(this.localTimezone);

    // check if appointment falls to the next day
    if (eastern.day() < local.day()) {
      return local.format('HH:mm') + ' (+1 day)';
    } else if (eastern.day() > local.day()) {
      return local.format('HH:mm') + ' (-1 day)';
    }

    // convert EST/EDT time to users timezone and return formatted hour
    return local.format('HH:mm');
  }

  weekDay(weekDay: string): string {
    return ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'][weekDay];
  }

  getButtonColor(tab: string) {
    return this.currentTab === tab ? 'primary' : '';
  }

  isCurrentTab(tab: string): boolean {
    return this.currentTab === tab;
  }

  ngOnInit() {
    this.loadAppointments();

    // initialize websocket for instant data
    this.appointmentSocket = this.appointmentService.getSocket()
      .subscribe(() => {
        this.loadAppointments();
      });

    this.userId$.pipe(
      take(1),
      concatMap(id => this.userService.getProfile(id))
    )
    .subscribe(
      res => {
        this.profile = res;
        this.localTimezone = this.getLocalTimezone();
      },
      err => console.log(err)
    );
  }

  ngOnDestroy() {
    this.appointmentSocket.unsubscribe();
  }
}
