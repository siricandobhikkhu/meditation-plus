import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared';
import { MeditationComponent } from './meditation.component';
import { MeditationListEntryComponent } from './list-entry/meditation-list-entry.component';
import { MeditationChartComponent } from './chart/meditation-chart.component';
import { EmojiModule } from '../emoji';
import { ProfileModule } from '../profile';
import { ChartModule } from 'primeng/chart';
import { StartMeditationComponent } from './start-meditation/start-meditation.component';
import { EffectsModule } from '@ngrx/effects';
import { MeditationEffect } from 'app/meditation/effects/meditation.effect';
import { OwnMeditationComponent } from './own-meditation/own-meditation.component';
import { TimeLeftPipe } from './time-left.pipe';

@NgModule({
  imports: [
    SharedModule,
    ProfileModule,
    FormsModule,
    RouterModule,
    EmojiModule,
    ChartModule,
    EffectsModule.forFeature([
      MeditationEffect
    ])
  ],
  declarations: [
    MeditationComponent,
    MeditationListEntryComponent,
    MeditationChartComponent,
    StartMeditationComponent,
    OwnMeditationComponent,
    TimeLeftPipe
  ],
  exports: [
    MeditationComponent,
    MeditationListEntryComponent,
    MeditationChartComponent
  ],
  entryComponents: [
    StartMeditationComponent
  ]
})
export class MeditationModule { }
