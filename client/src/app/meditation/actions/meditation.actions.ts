import { Action } from '@ngrx/store';
import { Meditation } from '../meditation';

export const LOAD = '[Meditation] Load';
export const LOAD_DONE = '[Meditation] Load Done';
export const LIKE = '[Meditation] Like';
export const LIKE_DONE = '[Meditation] Like Done';
export const POST = '[Meditation] Post';
export const POST_DONE = '[Meditation] Post Done';
export const CANCEL = '[Meditation] Delete';
export const CANCEL_DONE = '[Meditation] Delete Done';
export const WS_ON_MEDITATION = '[Meditation] WS On Meditation';

export class LoadMeditations implements Action {
  readonly type = LOAD;
}

export class LoadMeditationsDone implements Action {
  readonly type = LOAD_DONE;
  constructor(public payload: { meditations: Meditation[], currentUserId: string }) {}
}

export class LikeMeditations implements Action {
  readonly type = LIKE;
}

export class LikeMeditationsDone implements Action {
  readonly type = LIKE_DONE;
}

export class PostMeditation implements Action {
  readonly type = POST;
  constructor(public payload: {walking: number, sitting: number}) {}
}

export class PostMeditationDone implements Action {
  readonly type = POST_DONE;
}

export class CancelMeditation implements Action {
  readonly type = CANCEL;
}

export class CancelMeditationDone implements Action {
  readonly type = CANCEL_DONE;
}

export class WebsocketOnMeditation implements Action {
  readonly type = WS_ON_MEDITATION;
}

export type Actions = LoadMeditations
 | LoadMeditationsDone
 | PostMeditation
 | PostMeditationDone
 | LikeMeditations
 | LikeMeditationsDone
 | WebsocketOnMeditation
;
