import {
  Component,
  Input,
  ChangeDetectionStrategy,
  ViewChild,
  Output,
  EventEmitter,
  OnInit
} from '@angular/core';
import { MatMenuTrigger } from '@angular/material';
import { Message } from '../message';
import * as moment from 'moment';
import * as _ from 'lodash';
import { Store } from '@ngrx/store';
import { AppState } from '../../reducers';
import { Observable } from 'rxjs';
import { selectId } from '../../auth/reducders/auth.reducers';
import { take, filter } from 'rxjs/operators';
import { DialogService } from '../../dialog/dialog.service';

@Component({
  selector: 'message-list-entry',
  templateUrl: './message-list-entry.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: [
    './message-list-entry.component.styl'
  ]
})
export class MessageListEntryComponent implements OnInit {

  @Input() message: Message;
  @Input() admin = false;
  @Input() menuOpen = false;
  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;
  @Output() menuOpened: EventEmitter<any> = new EventEmitter<any>();
  @Output() menuClosed: EventEmitter<any> = new EventEmitter<any>();
  @Output() onDelete: EventEmitter<Message> = new EventEmitter<Message>();
  @Output() onUpdate: EventEmitter<Message> = new EventEmitter<Message>();
  localMenuOpen = false;

  userId$: Observable<string>;

  constructor(private dialog: DialogService, store: Store<AppState>) {
    this.userId$ = store.select(selectId);
  }

  public ngOnInit() {
    this.trigger.onMenuClose.subscribe(() => {
      this.menuClosed.emit();
      this.localMenuOpen = false;
    });
  }

  public showMenu() {
    this.userId$.pipe(take(1)).subscribe(id => {
      // prevent opening when other menu is opened or
      // if the message is deleted. Or if current user not creator
      // of message.
      if (this.menuOpen
        || this.message.deleted
        || (!this.message.user
          || id !== this.message.user._id)
        && !this.admin
      ) {
        return;
      }

      this.localMenuOpen = true;
      this.trigger.openMenu();
      this.menuOpened.emit();
    });
  }

  public delete() {
    this.dialog.confirmDelete().pipe(filter(val => !!val))
      .subscribe(() => this.onDelete.emit(this.message));
  }

  public closeMenu() {
    this.trigger.closeMenu();
    this.menuClosed.emit();
  }

  public edit() {
    this.dialog.input(
      'Update message',
      'Please enter your updated message.',
      'Message',
      this.message.text
    ).pipe(
      filter(val => !!val && this.message.text !== val),
    ).subscribe(val => {
      const newMessage = _.cloneDeep(this.message);
      newMessage.text = val;
      this.onUpdate.emit(newMessage);
    });
  }

  public editDone() {
    if (this.admin) {
      return false;
    }

    const created = moment(this.message.createdAt);
    const now = moment();
    const duration = moment.duration(now.diff(created));

    return duration.asMinutes() >= 30;
  }
}
