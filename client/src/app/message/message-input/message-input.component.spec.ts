import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageInputComponent } from './message-input.component';
import { MaterialModule } from '../../shared/material.module';
import { RouterTestingModule } from '@angular/router/testing';
import { MomentModule } from 'ngx-moment';
import { LinkyModule } from 'angular-linky';
import { EmojiModule } from '../../emoji';
import { UserTextListModule } from '../../user-text-list/user-text-list.module';
import { SharedModule } from '../../shared';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Store } from '@ngrx/store';
import { MockStore } from 'testing/mock.store';
import { AutocompleteMessageEffect } from '../effects/autocomplete-message.effect';
import { MockEffect } from 'testing/mock.effect';
import { Actions } from '@ngrx/effects';
import { MockActions } from 'testing/mock.actions';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

describe('MessageInputComponent', () => {
  let component: MessageInputComponent;
  let fixture: ComponentFixture<MessageInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MaterialModule,
        RouterTestingModule,
        MomentModule,
        LinkyModule,
        EmojiModule,
        UserTextListModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        NoopAnimationsModule
      ],
      providers: [
        {provide: Store, useClass: MockStore},
        {provide: AutocompleteMessageEffect, useClass: MockEffect},
        {provide: Actions, useClass: MockActions}
      ],
      declarations: [ MessageInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
