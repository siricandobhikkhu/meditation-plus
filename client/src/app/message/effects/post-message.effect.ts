import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { MessageService } from 'app/message/message.service';
import { concatMap, map } from 'rxjs/operators';
import { PostMessage, POST, PostMessageDone } from '../actions/message.actions';
import { of } from 'rxjs';

@Injectable()
export class PostMessageEffect {
  constructor(
    private actions$: Actions,
    private service: MessageService
  ) {
  }

  @Effect()
  post$ = this.actions$
    .ofType<PostMessage>(POST)
    .pipe(
      map(action => action.payload),
      concatMap(message => this.service.post(message)),
      concatMap(() => of(new PostMessageDone()))
    );
}
