import { Component, ApplicationRef, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppState } from '../reducers';
import { selectId, selectAdmin, selectProfile } from '../auth/reducders/auth.reducers';
import { filter, concatMap } from 'rxjs/operators';
import { MeetingService } from './meeting.service';
import * as $script from 'scriptjs';
import { DialogService } from '../dialog/dialog.service';
import { ApiConfig } from '../../api.config';

declare var gapi: any;
declare var JitsiMeetExternalAPI: any;

@Component({
  selector: 'meeting',
  templateUrl: './meeting.component.html',
  styleUrls: [
    './meeting.component.styl',
  ]
})
export class MeetingComponent {
  @ViewChild('jitsiWrapper', {read: ElementRef}) jitsiWrapper: ElementRef;

  loading = true;
  loadingInitiate = false;

  meeting: any;
  jitsi: any;

  initiateError: boolean;

  userId$: Observable<string>;
  admin$: Observable<boolean>;
  profile$: Observable<any>;

  constructor(
    private meetingService: MeetingService,
    private appRef: ApplicationRef,
    private dialog: DialogService,
    private router: Router,
    store: Store<AppState>
  ) {
    this.userId$ = store.select(selectId);
    this.admin$ = store.select(selectAdmin);
    this.profile$ = store.select(selectProfile);

    this.meetingService
      .getMeeting()
      .subscribe(
        res => {
          this.loading = false;
          this.meeting = res;

          if (res.initiated) {
            this.activateJitsi();
            this.activateHangoutsButton();
          }
        },
        () => this.loading = false
      );
  }

  initiate(): void {
    this.loadingInitiate = true;

    this.meetingService.initiate()
      .subscribe(
        () => {
          this.loadingInitiate = false;
          this.meeting.initiated = true;
          this.activateJitsi();
          this.activateHangoutsButton();
        },
        () => {
          this.loadingInitiate = false;
          this.initiateError = true;
        });
  }

  /**
   * Display Hangouts Button
   */
  activateHangoutsButton(): void {
    // initialize Google Hangouts Button
    $script('https://apis.google.com/js/platform.js', () => {
      // kick in Change Detection
      this.appRef.tick();

      gapi.hangout.render('hangout-button', {
        render: 'createhangout',
        invites: [{ 'id': 'yuttadhammo@gmail.com', 'invite_type': 'EMAIL' }],
        initial_apps: [{
          app_id: '211383333638',
          start_data: 'dQw4w9WgXcQ',
          app_type: 'ROOM_APP'
        }],
        widget_size: 175
      });
    });
  }

  activateJitsi(): void {
    $script('https://meet.jit.si/external_api.js', () => {
      // kick in Change Detection
      this.appRef.tick();

      this.jitsi = new JitsiMeetExternalAPI('meet.jit.si', {
        roomName: this.meeting.token,
        parentNode: this.jitsiWrapper.nativeElement,
        noSSL: false,
        interfaceConfigOverwrite: {
          SHOW_JITSI_WATERMARK: false,
          SHOW_WATERMARK_FOR_GUESTS: false,
          DISPLAY_WELCOME_PAGE_CONTENT: false,
          TOOLBAR_BUTTONS: [
            // main toolbar
            'microphone', 'camera', 'desktop', 'fullscreen', 'fodeviceselection', 'hangup',
            // extended toolbar
            'profile', 'contacts', 'chat', 'sharedvideo', 'settings', 'videoquality', 'filmstrip'
          ],
          SUPPORT_URL: 'https://gitlab.com/sirimangalo/meditation-plus/wikis/Troubleshooting'
        }
      });

      this.profile$.subscribe(res => {
        this.jitsi.executeCommand('displayName', res.name);
        this.jitsi.executeCommand('avatarUrl', `${ApiConfig.url}/public/${res.username}-${res.avatarImageToken}-192.jpeg`);
      });
    });
  }

  close(): void {
    if (!this.meeting) {
      return;
    }

    this.dialog
      .confirm(
        'Close Meeting',
        'Are you sure you want to close the meeting with ' + this.meeting.user.name + '?'
      )
      .pipe(
        filter(val => !!val),
        concatMap(() => this.meetingService.close(this.meeting._id))
      ).subscribe(() => this.router.navigate(['/schedule']));
  }
}
