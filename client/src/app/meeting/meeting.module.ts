import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../shared';
import { ProfileModule } from '../profile';
import { EmojiModule } from '../emoji';
import { MomentModule } from 'ngx-moment';
import { MeetingComponent } from './meeting.component';
import { UserTextListModule } from '../user-text-list/user-text-list.module';

const routes: Routes = [
  { path: '', component: MeetingComponent }
];

@NgModule({
  imports: [
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    MomentModule,
    ProfileModule,
    EmojiModule,
    UserTextListModule
  ],
  declarations: [
    MeetingComponent
  ],
  exports: [
    MeetingComponent
  ],
})
export class MeetingModule { }
