import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileChartComponent } from './profile-chart.component';
import { ChartModule } from 'primeng/chart';
import { MaterialModule } from '../../shared/material.module';

import { UserService } from '../../user/user.service';
import { FakeUserService } from '../../user/testing/fake-user.service';
import { AuthService } from '../../shared/auth.service';
import { FakeAuthService } from '../../shared/testing/fake-auth.service';

import { Store } from '@ngrx/store';
import { MockStore } from 'testing/mock.store';

describe('ProfileChartComponent', () => {
  let component: ProfileChartComponent;
  let fixture: ComponentFixture<ProfileChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ChartModule,
        MaterialModule
      ],
      declarations: [ ProfileChartComponent ],
      providers: [
        {provide: UserService, useClass: FakeUserService},
        {provide: AuthService, useClass: FakeAuthService},
        {provide: Store, useClass: MockStore}
     ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should rotate arrays correctly', () => {
    expect(component.rotateArray([], 2)).toEqual([]);
    expect(component.rotateArray([1], 2)).toEqual([1]);
    expect(component.rotateArray([3, 1, 2], 1)).toEqual([1, 2, 3]);
    expect(component.rotateArray([1, 2, 3], 3)).toEqual([1, 2, 3]);
    expect(component.rotateArray([3, 1, 2], 10)).toEqual([1, 2, 3]);
    expect(component.rotateArray([4, 5, 1, 2, 3], 2)).toEqual([1, 2, 3, 4, 5]);
  });
});
