import { Component, ChangeDetectionStrategy, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'badge',
  templateUrl: './badge.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: [
    './badge.component.styl'
  ]
})
export class BadgeComponent implements OnChanges {
  @Input() numberBadges: number;

  stars: Array<{title: string, level: number}> = [];

  mergeAt = 4;
  maxLevel = 4;

  ngOnChanges() {
    if (this.numberBadges > 0) {
      this.stars = [];
      this.mergeBadges(this.numberBadges);
    }
  }

  /**
   * Calculates the set of badges/stars for a given value of
   * consecutive days.
   */
  mergeBadges(numOfBadges: number): void {
    // Calculate the number of minutes the badge at the
    // current level stands for, meaning
    //   level 2 = 4 * level 1 = 4^(2-1) = 4         (Level 1 Badges)
    //   level 3 = 4 * level 2 = 4^(3-1) = 4^2 = 16  (Level 1 Badges)
    //   level 4 = 4 * level 3 = 4^(4-1) = 4^3 = 64  (Level 1 Badges)

    let remainingBadges = numOfBadges;
    for (let level = this.maxLevel; level >= 1; level--) {
      const numForOne = Math.pow(this.mergeAt, level - 1);

      if (remainingBadges >= numForOne) {
        const numBadgesLevel = Math.floor(remainingBadges / numForOne);

        for (let i = 0; i < numBadgesLevel; i++) {
          this.stars.push({
            level: level,
            title: level === 1
              ? '10 consecutive days'
              : this.mergeAt + ' badges of level ' + (level - 1).toString()
          });
        }

        remainingBadges -= numForOne * numBadgesLevel;
      }
    }
  }
}
