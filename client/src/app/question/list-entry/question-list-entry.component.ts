import {
  Component,
  Input,
  ChangeDetectionStrategy,
} from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../reducers';
import { selectId, selectAdmin } from '../../auth/reducders/auth.reducers';
import { Observable } from 'rxjs';
import { DialogService } from '../../dialog/dialog.service';
import { filter, tap, concatMap } from 'rxjs/operators';
import { MatDialog, MatSnackBar } from '@angular/material';
import { SuggestVideoUrlDialogComponent } from '../suggest-videourl-dialog/suggest-videourl-dialog.component';
import { LikeQuestion, AnswerQuestion, AnsweringQuestion, CancelAnsweringQuestion, DeleteQuestion } from '../actions/question.actions';
import { QuestionService } from '../question.service';

@Component({
  selector: 'question-list-entry',
  templateUrl: './question-list-entry.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: [
    './question-list-entry.component.styl'
  ]
})
export class QuestionListEntryComponent {

  @Input() question: any;
  @Input() mode: 'unanswered'|'answered'|'suggestion' = 'unanswered';

  loading = false;
  userId$: Observable<string>;
  showYoutubePlayer = false;
  isAdmin$: Observable<boolean>;

  constructor(
    private dialog: DialogService,
    private matDialog: MatDialog,
    private snackbar: MatSnackBar,
    private store: Store<AppState>,
    private questionService: QuestionService
  ) {
    this.userId$ = store.select(selectId);
    this.isAdmin$ = store.select(selectAdmin);
  }

  like() {
    this.loading = true;
    this.store.dispatch(new LikeQuestion(this.question));
  }

  answer() {
    this.loading = true;
    this.store.dispatch(new AnswerQuestion(this.question));
  }

  answering() {
    this.loading = true;
    this.store.dispatch(new AnsweringQuestion(this.question));
  }

  unanswering() {
    this.loading = true;
    this.store.dispatch(new CancelAnsweringQuestion(this.question));
  }

  delete() {
    this.dialog.confirmDelete().pipe(
      filter(val => !!val),
      tap(() => this.loading = true),
    ).subscribe(() => this.store.dispatch(new DeleteQuestion(this.question)));
  }

  suggestVideoUrl() {
    this.matDialog.open(SuggestVideoUrlDialogComponent, {
      data: { question: this.question }
    }).afterClosed()
      .pipe(
        filter(val => !!val),
        tap(() => this.loading = true),
        concatMap(val => this.questionService.suggestVideoUrl(this.question, val))
      ).subscribe(
        () => {
          this.loading = false;
          this.snackbar.open('Thank you for your suggestion. We will review it as soon as possible.');
        },
        err => {
          this.loading = false;
          this.snackbar.open(
            err.error || 'Error while saving your suggestion.'
          );
        }
      );
  }
}
