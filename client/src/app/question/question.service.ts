import { Injectable } from '@angular/core';
import { ApiConfig } from '../../api.config';
import { WebsocketService } from '../shared';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { concatMap } from 'rxjs/operators';
import { Question } from './question';

@Injectable()
export class QuestionService {

  public constructor(
    private http: HttpClient,
    private wsService: WebsocketService
  ) {
  }

  public getQuestions(
    filterAnswered = false,
    page = 0,
    searchParams = null
  ): Observable<Question[]> {
    let params = new HttpParams();
    params = params.append('filterAnswered', filterAnswered ? 'true' : 'false');
    params = params.append('page', '' + page);

    if (searchParams) {
      Object.keys(searchParams).map(p => params = params.append(p, searchParams[p].toString()));
    }

    return this.http.get<Question[]>(ApiConfig.url + '/api/question', { params });
  }

  public post(question: string): Observable<any> {
    return this.http.post(
      ApiConfig.url + '/api/question',
      JSON.stringify({ text: question })
    );
  }

  public like(question): Observable<any> {
    return this.http.post(
      ApiConfig.url + '/api/question/' + question._id + '/like',
      ''
    );
  }

  public delete(question): Observable<any> {
    return this.http.delete(
      ApiConfig.url + '/api/question/' + question._id
    );
  }

  public answer(question): Observable<any> {
    return this.http.post(
      ApiConfig.url + '/api/question/' + question._id + '/answer',
      ''
    );
  }

  public answering(question): Observable<any> {
    return this.http.post(
      ApiConfig.url + '/api/question/' + question._id + '/answering',
      ''
    );
  }

  public unanswering(question): Observable<any> {
    return this.http.post(
      ApiConfig.url + '/api/question/' + question._id + '/unanswering',
      ''
    );
  }

  public suggestVideoUrl(question, videoUrl): Observable<any> {
    return this.http.post(
      ApiConfig.url + '/api/question/' + question._id + '/suggest-url',
      JSON.stringify({ videoUrl })
    );
  }


  public findSuggestions(question: string): Observable<any> {
    return this.http.post(
      ApiConfig.url + '/api/question/suggestions',
      JSON.stringify({ text: question })
    );
  }

  public getCount(): Observable<any> {
    return this.http.get(ApiConfig.url + '/api/question/count');
  }

  /**
   * Initializes Socket.io client with Jwt and listens to 'question'.
   */
  public getSocket(): Observable<any> {
    return this.wsService.getSocket().pipe(
      concatMap(websocket => Observable.create(obs => {
        websocket.on('question', res => obs.next(res));
      }))
    );
  }
}
