import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'hasTimecode' })
export class HasTimecodePipe implements PipeTransform {

  transform(value: string): boolean {
    if (!value) {
      return false;
    }

    return new RegExp('t=[0-9]+', 'ig').test(value);
  }

}
