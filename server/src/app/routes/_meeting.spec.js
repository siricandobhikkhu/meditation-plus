import { app } from '../../server.conf.js';
import supertest from 'supertest';
import { expect } from 'chai';
import { AuthedSupertest } from '../helper/authed-supertest.js';
import Meeting from '../models/meeting.model.js';
import { User } from '../models/user.model.js';
import Settings from '../models/settings.model.js';
import Appointment from '../models/appointment.model.js';
import moment from 'moment-timezone';
import timekeeper from 'timekeeper';

const request = supertest(app);
let alice = new AuthedSupertest();
let mallory = new AuthedSupertest(
  'Mallory',
  'mallory',
  'mallory@sirimangalo.org',
  '4a56s4d6a5s4d6a5sd'
);
let teacher = new AuthedSupertest(
  'Admin User',
  'yuttadhammo',
  'admin@sirimangalo.org',
  'password',
  'ROLE_ADMIN',
  true
);


describe('Meeting Routes', () => {
  // eslint-disable-next-line no-unused-vars
  let appointment, settings, meeting;

  before(async () => {
    await Appointment.remove();
    await Settings.remove();
    await Meeting.remove();

    settings = await Settings.create({
      appointmentsTimezone: 'UTC'
    });
  });

  after(async () => {
    await User.remove();
    await Appointment.remove();
    await Settings.remove();
    await Meeting.remove();
    timekeeper.reset();
  });

  describe('GET /api/meeting', () => {
    timekeeper.travel(moment.tz('2018-03-10 10:00:00', 'UTC').toDate());

    teacher.authorize();
    alice.authorize();
    mallory.authorize();

    it('should respond with 401 when not authenticated', done => {
      request
        .get('/api/meeting')
        .expect(401)
        .end(err => done(err));
    });

    it('should respond with 403 if user has no appointment', done => {
      timekeeper.travel(moment.tz('2018-03-10 14:00:00', 'UTC').toDate());
      mallory
        .get('/api/meeting')
        .expect(403)
        .end(err => done(err));
    });

    it('should respond with 200 and the meeting if appointment holder requests in time', done => {
      timekeeper.travel(moment.tz('2018-03-10 14:00:00', 'UTC').toDate());
      Appointment.create({
        user: alice.user._id,
        weekDay: 6,
        hour: 1400
      }, () => alice
        .get('/api/meeting')
        .expect(200)
        .end((err, res) => {
          expect(res.body).to.have.property('user');
          expect(res.body).to.have.property('initiated');
          expect(res.body).to.have.property('token');
          expect(res.body.initiated).to.equal(false);
          expect(res.body.token.length > 0).to.equal(true);

          meeting = res.body;

          done(err);
        })
      );
    });

    it('should respond with 400 if teacher requests and meeting is not initiated', done => {
      timekeeper.travel(moment.tz('2018-03-10 14:00:00', 'UTC').toDate());
      teacher
        .get('/api/meeting')
        .expect(400)
        .end(err => done(err));
    });

    it('should respond with 200 if teacher requests and meeting is initiated', done => {
      timekeeper.travel(moment.tz('2018-03-10 14:00:00', 'UTC').toDate());

      Meeting
        .findOneAndUpdate(
          {},
          { initiated: true },
          () => teacher
            .get('/api/meeting')
            .expect(200)
            .end(err => done(err))
        );
    });
  });

  describe('GET /api/meeting/initiate', () => {
    before(() => timekeeper.reset());

    teacher.authorize();
    alice.authorize();
    mallory.authorize();

    it('should respond with 401 when not authenticated', done => {
      request
        .post('/api/meeting/initiate')
        .expect(401)
        .end(err => done(err));
    });

    it('should respond with 403 if user has no meeting (1)', done => {
      alice
        .post('/api/meeting/initiate')
        .expect(403)
        .end(err => done(err));
    });

    it('should respond with 403 if user has no meeting (2)', done => {
      Meeting
        .create({
          token: 'abc',
          user: alice.user._id,
          initiated: false
        }, () => mallory
          .post('/api/meeting/initiate')
          .expect(403)
          .end(err => done(err))
        );
    });

    it('should respond with 200 if user has meeting', done => {
      alice
        .post('/api/meeting/initiate')
        .expect(204)
        .end(err => done(err));
    });

    it('should respond with 403 if already initiated', done => {
      alice
        .post('/api/meeting/initiate')
        .expect(403)
        .end(err => done(err));
    });
  });
});
