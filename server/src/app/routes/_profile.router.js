import { writableFields, User } from '../models/user.model.js';
import { ProfileHelper } from '../helper/profile.js';
import { logger } from '../helper/logger.js';
import { getUser } from '../helper/get-user';
import multer from 'multer';
import { unlinkSync, existsSync } from 'fs';
import sharp from 'sharp';
import randomstring from 'randomstring';
import readChunk from 'read-chunk';
import fileType from 'file-type';
import config from '../../config/config';


let ObjectId = require('mongoose').Types.ObjectId;

const getProfile = async (query, req, res) =>  {
  try {
    let doc = await getUser(query);

    if (!doc) return res.sendStatus(404);

    return res.json(doc);
  } catch (err) {
    logger.error(req.url, err);
    return res.status(400).send(err);
  }
};

// TODO: Think about adding this to config
const avatarAllowedMimeTypes = [
  'image/jpeg', 'image/png'
];
const avatarUpload = multer({
  dest: config.UPLOAD_DIR,
  limits: {
    fields: 3,
    fileSize: 2097152 // 2MB
  },
  fileFilter: (req, file, cb) => {
    if (!avatarAllowedMimeTypes.includes(file.mimetype.toLowerCase())) {
      return cb({ message: 'File is not a valid image.' }, false);
    }

    // accept file
    cb(null, true);
  }
}).single('avatar');

const avatarSizes = [320, 192, 160, 80, 40];

export default (app, router) => {

  /**
   * @api {get} /api/profile Get profile details
   * @apiName GetProfile
   * @apiGroup Profile
   * @apiDescription Get the profile data of the currently logged in user.
   *
   * @apiSuccess {Object}     local             Details for local authentication
   * @apiSuccess {String}     local.email       Email address
   * @apiSuccess {String}     name              Name
   * @apiSuccess {Boolean}    showEmail         Show email publicly
   * @apiSuccess {String}     description       Profile description
   * @apiSuccess {String}     website           Website
   * @apiSuccess {String}     country           Country
   * @apiSuccess {String}     avatarImageToken  Token for getting profile image
   */
  router.get('/api/profile', async (req, res) => {
    try {
      let doc = await User
        .findOne({
          _id: req.user._id
        })
        .lean()
        .exec();

      delete doc.local['password'];
      delete doc['__v'];

      res.json(doc);
    } catch (err) {
      logger.error(req.url, err);
      res.status(400).send(err);
    }
  });

  /**
   * @api {get} /api/profile/:id Get profile details of a user
   * @apiName ShowProfile
   * @apiGroup Profile
   * @apiDescription Get the profile data of a user.
   *
   * @apiSuccess {Object}     local             Details for local authentication
   * @apiSuccess {String}     local.email       Email address (if public)
   * @apiSuccess {String}     name              Name
   * @apiSuccess {String}     description       Profile description
   * @apiSuccess {String}     website           Website
   * @apiSuccess {String}     country           Country
   * @apiSuccess {Object}     meditations       Last week meditation times
   * @apiSuccess {String}     avatarImageToken  Token for getting profile image
   */
  router.get('/api/profile/:id', async (req, res) => {
    return getProfile({ '_id': ObjectId(req.params.id) }, req, res);
  });

  /**
   * @api {get} /api/profile/username/:username Get profile details of a user by username
   * @apiName ShowProfileByUsername
   * @apiGroup Profile
   * @apiDescription Get the profile data of a user.
   *
   * @apiSuccess {Object}     local             Details for local authentication
   * @apiSuccess {String}     local.email       Email address (if public)
   * @apiSuccess {String}     name              Name
   * @apiSuccess {String}     description       Profile description
   * @apiSuccess {String}     website           Website
   * @apiSuccess {String}     country           Country
   * @apiSuccess {Object}     meditations       Last week meditation times
   * @apiSuccess {String}     avatarImageToken  Token for getting profile image
   */
  router.get('/api/profile/username/:username', async (req, res) => {
    return getProfile({ 'username': req.params.username }, req, res);
  });

  /**
   * @api {get} /api/profile/stats/:usernameOrId Get profile statistics of a user by username or id
   * @apiName ShowStats
   * @apiGroup Profile
   * @apiDescription Get the profile statistics of a user.
   *
   * @apiSuccess {Object}     general           Generals meditation stats
   * @apiSuccess {Object}     chartData         Historical meditation stats for
   *                                            three different chart types
   * @apiSuccess {Object}     consecutiveDays   Number of current and total consecutive
   *                                            days of meditation
   */
  router.get('/api/profile/stats/:usernameOrId/:monthOffset?', async (req, res) => {
    try {
      const user = await User.findOne({
        $or: [
          { _id: ObjectId.isValid(req.params.usernameOrId) ? ObjectId(req.params.usernameOrId) : null },
          { username: req.params.usernameOrId }
        ]
      });

      if (user && user.hideStats && user._id.toString() !== req.user._id &&
        req.user.role !== 'ROLE_ADMIN') {
        return res.sendStatus(403);
      }

      if (!user) {
        return res.sendStatus(404);
      }

      const pHelper = new ProfileHelper(user);

      const monthOffset = req.params.monthOffset ? parseInt(req.params.monthOffset, 10) : -1;
      if (!isNaN(monthOffset) && monthOffset >= 0) {
        // only return month data if offset specified
        return res.json(await pHelper.getMonthChartData(monthOffset));
      }

      const result = {
        general: await pHelper.getGeneralStats(user),
        chartData: await pHelper.getChartData(user),
        consecutiveDays: await pHelper.getConsecutiveDays(user)
      };

      res.json(result);
    } catch (err) {
      logger.error(req.url, err);
      res.status(400).send(err);
    }
  });

  /**
   * @api {put} /api/profile Update profile
   * @apiName UpdateProfile
   * @apiGroup Profile
   * @apiDescription Updating the profile of the currently logged in user.
   *
   * @apiParam {Boolean}    showEmail       Show email publicly
   * @apiParam {String}     description     Profile description
   * @apiParam {String}     website         Website
   * @apiParam {String}     country         Country
   */
  router.put('/api/profile', async (req, res) => {

    if (!req.body.local) {
      return res.status(400).send('Missing local field');
    }

    try {
      let user = await User.findById(req.user._id);

      // change password if set
      if (req.body.newPassword && req.body.newPassword.length >= 8
        && req.body.newPassword.length <= 128) {
        user.local.password = user.generateHash(req.body.newPassword);
        delete req.body.newPassword;
      }

      for (const key of Object.keys(req.body)) {
        if (key === 'local' && req.body.local.email) {
          user.local.email = req.body.local.email;
          continue;
        }

        if (key === 'isTeacher' && req.user.role === 'ROLE_ADMIN') {
          user.isTeacher = req.body.isTeacher;
          continue;
        }

        if (!writableFields.includes(key)) {
          continue;
        }

        user[key] = req.body[key];
      }

      await user.save();

      res.sendStatus(204);
    } catch (err) {
      logger.error(req.url, err);
      res.status(400).send(err);
    }
  });

  /**
   * @api {put} /api/profile/avatar/upload Upload a new avatar image
   * @apiName UploadAvatar
   * @apiGroup Profile
   * @apiDescription Upload a new image used as profile avatar.
   *
   * @apiParam {Buffer}     avatar        Uploaded image file
   * @apiParam {String}     crop          JSON string of cropping options
   *
   * @apiSuccess {String}   avatarSecret  Token for accessing the avatar image on public route.
   */
  router.post('/api/profile/avatar/upload', async (req, res) => avatarUpload(req, res, async err => {
    try {
      if (err) {
        if (req.file && req.file.path && existsSync(req.file.path)) {
          unlinkSync(req.file.path);
        }

        return res.status(400).json({ error: err.message });
      }

      // additional file type checking by reading
      // the first 16 bytes for security reasons.
      const buffer = await readChunk(req.file.path, 0, 32);
      const fileInfo = fileType(buffer);

      if (!avatarAllowedMimeTypes.includes(fileInfo.mime.toLocaleLowerCase())) {
        unlinkSync(req.file.path);
        return res.status(400).json({ error: 'This file format is not allowed. Only "jpeg" or "png" are valid.' });
      }

      let image = sharp(req.file.path);

      // security check for images which have a faked image size
      // which could be used for a DOS attack because the image
      // manipulation program allocates more memory.
      // See: https://github.com/danielmiessler/SecLists/tree/master/Payloads
      const imageMetadata = await image.metadata();

      if (imageMetadata.width > 2000 || imageMetadata.height > 2000) {
        unlinkSync(req.file.path);
        res.status(400).json({ error: 'The image size is too big (max. 2000x2000).' });
      }

      const user = await User.findOne({ _id: req.body.id && req.user.role === 'ROLE_ADMIN'
        ? req.body.id
        : req.user._id
      });

      // generate token for requesting via public route
      const avatarSecret = randomstring.generate({
        length: 32,
        capitalization: 'lowercase'
      });

      // apply custom cropping of user
      if (req.body.crop) {
        const cropData = JSON.parse(req.body.crop);
        image = image.extract(cropData);
      }

      // process image to fixed sizes and optimize
      await Promise.all(avatarSizes.map(async size => {
        await image
          .resize(size, size)
          .crop()
          .jpeg()
          .toFile(`${config.PUBLIC_DIR}/${user.username}-${avatarSecret}-${size}.jpeg`);
      }));

      const oldToken = user.avatarImageToken;
      await user.update({ avatarImageToken: avatarSecret });

      // remove upload file which is not used anymore and old files
      unlinkSync(req.file.path);

      // remove old avatar image if exists
      avatarSizes.map(size => {
        const tmpFilename = `${config.PUBLIC_DIR}/${user.username}-${oldToken}-${size}.jpeg`;
        if (existsSync(tmpFilename)) {
          unlinkSync(tmpFilename);
        }
      });

      res.json({ avatarSecret });
    } catch (err) {
      if (req.file && req.file.path && existsSync(req.file.path)) {
        unlinkSync(req.file.path);
      }

      logger.error(req.url, err);
      res.status(500).send('Internal Server Error');
    }
  }));

  /**
   * @api {delete} /api/profile/avatar/id Delete avatar
   * @apiName DeleteAvatar
   * @apiGroup Profile
   * @apiDescription Deleting a profile's avatar image. Only an admin can delete the image of a different user.
   *
   * @apiParam {String} id  Id of user whose image should be deleted
   */
  router.delete('/api/profile/avatar/:id', async (req, res) => {
    try {
      if (req.params.id !== req.user._id.toString() && req.user.role !== 'ROLE_ADMIN') {
        return res.sendStatus(403);
      }

      const user = await User.findOne({ _id: req.params.id });

      if (!user) {
        return res.sendStatus(400);
      }

      if (!user.avatarImageToken) {
        return res.sendStatus(204);
      }

      for (const size of avatarSizes) {
        unlinkSync(`${config.PUBLIC_DIR}/${user.username}-${user.avatarImageToken}-${size}.jpeg`);
      }

      await user.update({ avatarImageToken: '' });

      res.sendStatus(204);
    } catch (err) {
      logger.error(req.url, err);
      res.status(500).send(err);
    }
  });

  /**
   * @api {delete} /api/profile Delete account
   * @apiName DeleteProfile
   * @apiGroup Profile
   * @apiDescription Deleting the profile of the currently logged in user.
   */
  router.delete('/api/profile', async (req, res) => {
    try {
      const user = await User.findById(req.user._id);
      await user.remove();
      req.logout();
      req.session = null;
      res.sendStatus(204);
    } catch (err) {
      logger.error(req.url, err);
      res.status(500).send(err);
    }
  });
};
