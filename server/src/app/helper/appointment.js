import Settings from '../models/settings.model.js';
import Appointment from '../models/appointment.model.js';
import moment from 'moment-timezone';

let appointmentHelper = {
  /**
   * Converts Date or moment object to Integer representing the time.
   *
   * @param  {moment/Date}   Date or moment object
   * @return {Number}        Time as Number
   */
  timeToNumber: time => time instanceof Date
    ? time.getHours() * 100 + time.getMinutes()
    : (time instanceof moment ? time.hours() * 100 + time.minutes() : 0),

  /**
   * Parses hour as String.
   *
   * @param  {Number} hour Number representing time
   * @return {String}      'HH:mm' formatted time
   */
  printHour: hour => {
    const hourStr = '0000' + hour.toString();
    return hourStr.substr(-4, 2) + ':' + hourStr.substr(-2, 2);
  }
};

/**
 * Find an appointment for the given date/time.
 *
 * @param      {Date/Moment}          datetime         The date, moment, date-string or 'now'
 * @param      {number}               toleranceBefore  The minutes of tolerance before
 * @param      {number}               toleranceAfter   The minutes tolerance after
 * @return                                             The appointment object
 */
appointmentHelper.getNow = async (user, toleranceBefore = 5, toleranceAfter = 5) => {
  if (!user) {
    return null;
  }

  // load settings entity
  const settings = await Settings.findOne();
  let now = moment.tz(settings.appointmentsTimezone);

  if (!now.isValid()) {
    return null;
  }

  const minDate = now.clone().subtract(toleranceAfter, 'minutes');
  const maxDate = now.clone().add(toleranceBefore, 'minutes');

  let doc = null;
  if (minDate.weekday() !== maxDate.weekday()) {
    // handle the case where the tolerance range
    // includes two days
    doc = await Appointment
      .findOne({
        user: user._id,
        $or: [
          {
            weekDay: minDate.weekday(),
            hour: {
              $gte: appointmentHelper.timeToNumber(minDate)
            }
          },
          {
            weekDay: maxDate.weekday(),
            hour: {
              $lte: appointmentHelper.timeToNumber(maxDate)
            }
          }
        ]
      })
      .populate('user', 'name username avatarImageToken')
      .exec();
  } else {
    doc = await Appointment
      .findOne({
        user: user._id,
        weekDay: minDate.weekday(),
        hour: {
          $gte: appointmentHelper.timeToNumber(minDate),
          $lte: appointmentHelper.timeToNumber(maxDate)
        }
      })
      .populate('user', 'name username avatarImageToken')
      .exec();
  }

  return doc;
};

export default appointmentHelper;
