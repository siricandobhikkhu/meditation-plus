import mongoose from 'mongoose';

let schema = mongoose.Schema({
  started: Date,
  ended: Date,
  videoUrl: { type: String, unique: true }
}, {
  timestamps: true
});

export default mongoose.model('Broadcast', schema);
