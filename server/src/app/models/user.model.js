import mongoose from 'mongoose';
import bcrypt from 'bcryptjs';
import Message from './message.model';
import Appointment from './appointment.model';
import Meditation from './meditation.model';
import Question from './question.model';
import Testimonial from './testimonial.model';

let userSchema = mongoose.Schema({
  username: {
    type: String,
    unique: true,
    index: true,
    sparse: true, // allow null
    validate: /^[a-zA-Z][a-zA-Z0-9-_.]{3,20}$/
  },
  local : {
    password : String,
    email : { type : String, unique : true }
  },
  avatarImageToken: String,
  suspendedUntil: Date,
  name: { type: String, maxlength: 30 },
  role : { type : String },
  showEmail: Boolean,
  hideStats: Boolean,
  description: { type: String, maxlength: 300 },
  website: { type: String, maxlength: 100 },
  country: String,
  lastActive: Date,
  lastMeditation: { type: Date, required: true, default: new Date(0) },
  lastLike: Date,
  sound: { type: String, default: '/assets/audio/bell1.mp3' },
  stableBell: { type: Boolean, default: false },
  timezone: String,
  verified: { type: Boolean, default: false },
  acceptedGdpr: { type: Boolean, default: false },
  acceptedGdprDate: Date,
  verifyToken: String,
  recoverUntil: Date,
  notifications: {
    livestream: Boolean,
    message: { type: Boolean, default: true },
    meditation: Boolean,
    question: { type: Boolean, default: true },
    // relevant for admins only
    testimonial: Boolean,
    appointment: [{ type: mongoose.Schema.Types.ObjectId, ref: 'PushSubscriptions' }]
  },
  meetings: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Meeting' }],
  isTeacher: Boolean
}, {
  timestamps: true
});

userSchema.methods.generateHash = function(password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8));
};

userSchema.methods.validPassword = function(password) {
  return bcrypt.compareSync(password, this.local.password);
};

userSchema.pre('remove', async function(next) {
  await Message.remove({ user: this._id }).exec();
  await Appointment.remove({ user: this._id }).exec();
  await Meditation.remove({ user: this._id }).exec();
  await Question.remove({ user: this._id }).exec();
  await Testimonial.remove({ user: this._id }).exec();
  next();
});

const User = mongoose.model('User', userSchema);

const writableFields = [
  'username', 'name', 'hideStats',
  'description', 'website', 'country',
  'sound', 'stableBell', 'timezone',
  'notifications', 'showEmail'
];

const visiblePublicFields = [
  '_id', 'createdAt', 'avatarImageToken', ...writableFields
];

export { User, writableFields, visiblePublicFields };
