# Meditation+ App

![Screenshot of meditation tab](https://raw.githubusercontent.com/Sirimangalo/meditation-plus-angular/master/src/assets/img/screenshot.jpg)
![Screenshot of doing meditation](https://raw.githubusercontent.com/Sirimangalo/meditation-plus-angular/master/src/assets/img/screenshot2.jpg)
![Screenshot of profile](https://raw.githubusercontent.com/Sirimangalo/meditation-plus-angular/master/src/assets/img/screenshot3.jpg)

## Client

Angular Client for Meditation+ REST API.

### Configuration
Add a `client/src/api.config.ts`:

```js
export let ApiConfig = {
  url: 'http://localhost:3002' // the REST endpoint
};
```

### Install dependencies and run
```bash
# inside "client" of the cloned repository
$ cd client

# install dependencies
$ yarn

# run client
$ yarn run start
```

### Testing Development Tips
To isolate a spec file per run, change the function name `it` to `fit` or `describe` to `fdescribe`.

## Server
REST API built with Node, Express, PassportJS, MongoDB, Mongoose and Socket.io.

### Quick Start

```bash
# inside "server" of the cloned repository
$ cd server

# install dependencies
$ yarn

# run server
$ yarn run start
```

### API Documentation
You can view the latest API Documention in the [Build artifacts](https://gitlab.com/sirimangalo/meditation-plus/-/jobs/artifacts/master/file/server/doc/index.html?job=build:dev).

### config.json

The `server.conf.js` file is expecting certain `environment` `variables` to be set within `Node`. The `env.conf.js` has functions to check whether the expected `environment` `variables` have been setup before proceeding to start up the rest of the server. It uses a file called `config.json` stored in the `config` directory that looks something like this:

```
{
  "HOST" : "https://medidation.sirimangalo.org",
  "ENV" : "development",
  "PORT" : 3000,
  "MONGO_URI" : {
    "DEVELOPMENT" : "mongodb://[username:password]@host[:port]",
    "PRODUCTION" : "mongodb://[username:password]@host[:port]",
    "TEST" : "mongodb://[username:password]@host[:port]"
  },
  # Generate your own 256-bit WEP key here:
  # http://randomkeygen.com/
  # Note that you don't need to use specifically
  # this, but it will certainly suffice
  "SESSION_SECRET" : "the session secrect",
  # You can generate a key with a Google account at https://console.developers.google.com/
  # (needs Google Cloud Messaging and YouTube Data API to be activated first)
  "GOOGLE_API_KEY" : "the google api key"
}
```

### Development Data

Run the following commands to load some test data into the database:
```bash
$ cd server/src/scripts
$ node populate-runner.js
```
You can now login with the username `admin` and the password `password`.